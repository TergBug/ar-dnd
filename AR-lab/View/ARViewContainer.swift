import SwiftUI
import RealityKit
import ARKit

struct ARViewContainer: UIViewRepresentable {
    
    @ObservedObject var params: Params
    
    var arView = ARView(frame: .zero)
    
    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
    
    class Coordinator: NSObject, ARSessionDelegate {
        
        var parent: ARViewContainer
        
        init(parent: ARViewContainer) {
            self.parent = parent
        }
        
        func session(_ session: ARSession, didAdd anchors: [ARAnchor]) {
            for anchor in anchors {
                if let imAnchor = anchor as? ARImageAnchor {
                    let unit = createUnit()
                    placeOnMap(unit: unit, imAnchor: imAnchor, scaleFactor: 0)
                }
            }
        }
        
        func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
            for anchor in anchors {
                if let imAnchor = anchor as? ARImageAnchor {
                    let s = Float(parent.params.size)/1000
                    let unit = createUnit()
                    placeOnMap(unit: unit, imAnchor: imAnchor, scaleFactor: s)
                }
            }
        }
        
        private func createUnit() -> ModelEntity {
//            let box = MeshResource.generateBox(width: width, height: width, depth: width)
//            let shader = SimpleMaterial(color: .red, roughness: 4, isMetallic: true)
            let entity = try! ModelEntity.loadModel(named: "test")
            return entity
        }
        
        private func placeOnMap(unit: ModelEntity, imAnchor: ARImageAnchor, scaleFactor: Float) {
            let expectedSize = getExpectedObjSize(imAnchor: imAnchor)
            let realSize = getRealObjSize(unit: unit)
            let scale = scaleFactor + (expectedSize / realSize)
            unit.setScale(SIMD3(x: scale, y: scale, z: scale), relativeTo: nil)
            
            let objSizeX = ((unit.model?.mesh.bounds.max.x)! - (unit.model?.mesh.bounds.min.x)!) * scale
            let objSizeY = ((unit.model?.mesh.bounds.max.y)! - (unit.model?.mesh.bounds.min.y)!) * scale
            let objSizeZ = ((unit.model?.mesh.bounds.max.z)! - (unit.model?.mesh.bounds.min.z)!) * scale
            let factor = Float(100)
            var x = Float(parent.params.x) / factor
            var z = Float(parent.params.y) / factor
            let minX = -((Float(imAnchor.referenceImage.physicalSize.width) / 2) - (objSizeX / 2))
            let minZ = -((Float(imAnchor.referenceImage.physicalSize.height) / 2) - (objSizeZ / 2))
            let maxX = (Float(imAnchor.referenceImage.physicalSize.width) / 2) - (objSizeX / 2)
            let maxZ = (Float(imAnchor.referenceImage.physicalSize.height) / 2) - (objSizeZ / 2)
            if x >= maxX { x = maxX } else if x <= minX { x = minX }
            if z >= maxZ { z = maxZ } else if z <= minZ { z = minZ }
            parent.params.x = x * factor
            parent.params.y = z * factor
            
            let anchorEntity = AnchorEntity(anchor: imAnchor)
            unit.setPosition(SIMD3(x: x, y: objSizeY / 2, z: z), relativeTo: anchorEntity)
            
            anchorEntity.addChild(unit)
            parent.arView.scene.anchors.removeAll()
            parent.arView.scene.addAnchor(anchorEntity)
        }
        
        private func getRealObjSize(unit: ModelEntity) -> Float {
            let meshSizeX = (unit.model?.mesh.bounds.max.x)! - (unit.model?.mesh.bounds.min.x)!
            let meshSizeY = (unit.model?.mesh.bounds.max.y)! - (unit.model?.mesh.bounds.min.y)!
            let meshSizeZ = (unit.model?.mesh.bounds.max.z)! - (unit.model?.mesh.bounds.min.z)!
            return max(meshSizeX, meshSizeY, meshSizeZ)
        }
        
        private func getExpectedObjSize(imAnchor: ARImageAnchor) -> Float {
            let imWidth = Float(imAnchor.referenceImage.physicalSize.width)
            let imHeight = Float(imAnchor.referenceImage.physicalSize.height)
            return min(imWidth, imHeight) / 4
        }
        
    }
    
    func makeUIView(context: Context) -> ARView {
        arView.session.delegate = context.coordinator
        
        startImageTracking()
        return arView
    }
    
    func updateUIView(_ uiView: ARView, context: Context) { }
    
    private func startImageTracking() {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "GameMap", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        
        let config = ARImageTrackingConfiguration()
        config.trackingImages = referenceImages
        config.isAutoFocusEnabled = true
        config.maximumNumberOfTrackedImages = 1
        
        arView.session.run(config)
    }
    
}
