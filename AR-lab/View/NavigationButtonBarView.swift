import SwiftUI

struct NavigationButtonBarView: View {
    
    var params: Params
    
    var body: some View {
        VStack(spacing: 0) {
            HStack(alignment: .center, spacing: 50) {
                
                Button {
                    params.x -= 1
                } label: {
                    Image(systemName: "chevron.left")
                }

                Button {
                    params.y -= 1
                } label: {
                    Image(systemName: "chevron.up")
                }
                
                Button {
                    params.y += 1
                } label: {
                    Image(systemName: "chevron.down")
                }
                
                Button {
                    params.x += 1
                } label: {
                    Image(systemName: "chevron.right")
                }
            }
            .font(.system(size: 32))
            .foregroundColor(.white)
            .frame(width: UIScreen.main.bounds.width, height: 80, alignment: .center)
            .background(Color.black)
            .opacity(0.87)
            HStack(alignment: .center, spacing: 50) {
                
                Button { // Decrement Button
                    params.size -= 1
                    print("Tap -- : \(params.size)")
                } label: {
                    Image(systemName: "minus.diamond")
                }

                Button { // Reset Button
                    params.size = 0
                    print("Tap    : \(params.size)")
                } label: {
                    Image(systemName: "xmark.diamond.fill")
                }
                
                Button { // Increment Button
                    params.size += 1
                    print("Tap ++ : \(params.size)")
                } label: {
                    Image(systemName: "plus.diamond")
                }
            }
            .padding(.bottom, 15)
            .font(.system(size: 32))
            .foregroundColor(.white)
            .frame(width: UIScreen.main.bounds.width, height: 80, alignment: .center)
            .background(Color.black)
            .opacity(0.87)
        }
    }
    
}
