import SwiftUI
import RealityKit

struct ContentView : View {
    var count = Params()
    
    var body: some View {
        ZStack {
            ARViewContainer(params: count)
            VStack {
                Spacer()
                NavigationButtonBarView(params: count)
            }
        }
        .edgesIgnoringSafeArea(.all)
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 12 Pro")
    }
}
#endif
