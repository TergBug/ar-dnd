import Foundation

class Params: ObservableObject {
    
    @Published var size: Float = 0
    
    @Published var x: Float = 0
    @Published var y: Float = 0
    
}
